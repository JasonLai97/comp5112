# Start with a base image containing Java runtime (Here is the Java 11 base image)
FROM adoptopenjdk:11-jdk-hotspot

# Add a volume pointing to /tmp (Optional)
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=target/*.jar

# Add the application's jar to the container
ADD ${JAR_FILE} app.jar

# Run the jar file
ENTRYPOINT ["java","-jar","/app.jar"]