package com.comp5112.postgresDemo.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.sql.Connection
import java.sql.DriverManager

@Configuration
class JDBCDbConfig (
    @Value("\${spring.datasource.url}")
    private val dbUrl: String,
    @Value("\${spring.datasource.username}")
    private val dbUsername: String,
    @Value("\${spring.datasource.password}")
    private val dbPassword: String,
) {
    @Bean
    fun getConnection(): Connection {
        return DriverManager.getConnection(dbUrl, dbUsername, dbPassword)
    }
}