package com.comp5112.postgresDemo.config

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@OpenAPIDefinition
class SwaggerConfig {
    @Bean
    fun customOpenApi(): OpenAPI {
        return OpenAPI()
            .components(
                Components().addSecuritySchemes("basicScheme",
                    SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("basic"))
            )
            .info(
                Info().title("API Documentation")
                    .description("API description")
                    .version("v1")
            )
            .addSecurityItem(SecurityRequirement().addList("basicScheme"))
    }
}