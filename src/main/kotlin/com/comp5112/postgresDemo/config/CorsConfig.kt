package com.comp5112.postgresDemo.config

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter

@Configuration
class CorsConfig (
        @Value("\${spring.cross-origin.enable}")
        private val enableCrossOrigin: Boolean,
) {
    val log: Logger = LoggerFactory.getLogger(javaClass)

    @Bean
    fun corsFilter(): CorsFilter {
        if (!enableCrossOrigin) {
            log.info("CORS disabled")
        }

        val source = UrlBasedCorsConfigurationSource()
        val config = CorsConfiguration()

        // Allow all origins
        config.allowCredentials = true
        config.addAllowedOrigin("*") // <- this allows all origins

        config.addAllowedHeader("*")
        config.addAllowedMethod("*")

        source.registerCorsConfiguration("/**", config)
        return CorsFilter(source)
    }
}