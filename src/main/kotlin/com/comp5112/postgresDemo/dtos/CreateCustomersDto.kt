package com.comp5112.postgresDemo.dtos

data class CreateCustomersDto(
    val customerName: String,
    val vipFlg: Boolean,
    val phone: String,
    val email: String,
    val address: String,
)
