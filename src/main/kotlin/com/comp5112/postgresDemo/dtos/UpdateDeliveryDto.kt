package com.comp5112.postgresDemo.dtos

import java.time.LocalDateTime

data class UpdateDeliveryDto(
    val actualDeliveryDate: LocalDateTime,
    val delivered: Boolean,
)
