package com.comp5112.postgresDemo.dtos

import java.math.BigDecimal

data class CreateItemsDto(
    val itemPrice: Long,
    val discount: BigDecimal,
    val quantity: Int,
)
