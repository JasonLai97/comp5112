package com.comp5112.postgresDemo.dtos

import java.time.LocalDateTime

data class CustomersDto(
    val customerId: Long,
    val customerName: String,
    val vipFlg: Boolean,
    val phone: String,
    val email: String,
    val address: String,
    val orders: List<OrdersDto>?,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime,
)
