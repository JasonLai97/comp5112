package com.comp5112.postgresDemo.dtos

import com.comp5112.postgresDemo.util.udts.StatusUdt
import java.time.LocalDateTime

data class CreateOrdersDto(
    val orderDate: LocalDateTime,
    val status: StatusUdt.OrderStatus,
    val transaction: CreateTransactionDto,
)
