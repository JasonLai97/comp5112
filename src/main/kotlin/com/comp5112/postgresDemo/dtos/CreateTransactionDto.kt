package com.comp5112.postgresDemo.dtos

import java.time.LocalDateTime

data class CreateTransactionDto(
    val transactionDate: LocalDateTime,
    val method: String,
    val pickUpFlg: Boolean,
    val deliveryFlg: Boolean,
    val invoice: CreateInvoiceDto,
)
