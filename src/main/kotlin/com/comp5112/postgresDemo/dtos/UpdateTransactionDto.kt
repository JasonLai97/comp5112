package com.comp5112.postgresDemo.dtos

data class UpdateTransactionDto(
    val pickUpFlg: Boolean,
    val deliveryFlg: Boolean,
)
