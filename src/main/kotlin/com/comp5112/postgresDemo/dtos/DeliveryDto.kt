package com.comp5112.postgresDemo.dtos

import java.time.LocalDateTime

data class DeliveryDto(
    val deliveryId: Long,
    val address: String,
    val expectedDeliveryDate: LocalDateTime,
    val actualDeliveryDate: LocalDateTime,
    val delivered: Boolean,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime,
)
