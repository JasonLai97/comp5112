package com.comp5112.postgresDemo.dtos

import java.math.BigDecimal
import java.time.LocalDateTime

data class InvoiceDto(
    val invoiceId: Long,
    val paymentRef: Int,
    val refNo: Int?,
    val invoiceDate: LocalDateTime,
    val remark: String?,
    val taxable: Boolean,
    val tax: BigDecimal?,
    val totalAmount: Long,
    val items: List<ItemsDto>?,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime,
)
