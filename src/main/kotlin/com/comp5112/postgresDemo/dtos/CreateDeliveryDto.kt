package com.comp5112.postgresDemo.dtos

import java.time.LocalDateTime

data class CreateDeliveryDto(
    val address: String,
    val expectedDeliveryDate: LocalDateTime,
    val actualDeliveryDate: LocalDateTime,
    val delivered: Boolean,
)
