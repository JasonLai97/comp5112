package com.comp5112.postgresDemo.dtos

import java.math.BigDecimal
import java.time.LocalDateTime

data class ItemsDto(
    val itemId: Long,
    val itemPrice: Long,
    val discount: BigDecimal,
    val quantity: Int,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime,
)
