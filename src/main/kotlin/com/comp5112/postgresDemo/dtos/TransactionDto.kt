package com.comp5112.postgresDemo.dtos

import com.comp5112.postgresDemo.entities.Invoice
import java.time.LocalDateTime

data class TransactionDto(
    val transactionId: Long,
    val transactionDate: LocalDateTime,
    val method: String,
    val pickUpFlg: Boolean,
    val deliveryFlg: Boolean,
    val delivery: List<DeliveryDto>?,
    val invoice: Invoice,
    val createdAt: LocalDateTime,
    val updatedAt: LocalDateTime,
)
