package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.TransactionDto
import com.comp5112.postgresDemo.dtos.UpdateTransactionDto

interface TransactionController {
    fun updateTransaction(updateTransactionDto: UpdateTransactionDto, transactionId: Long): TransactionDto
}