package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.CreateCustomersDto
import com.comp5112.postgresDemo.dtos.CustomersDto
import com.comp5112.postgresDemo.mappers.CustomersMapper
import com.comp5112.postgresDemo.services.CustomersService
import com.comp5112.postgresDemo.util.toPage
import io.swagger.v3.oas.annotations.Operation
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
class CustomerControllerImpl(
    private val customersMapper: CustomersMapper,
    private val customersService: CustomersService,
): CustomerController {
    @GetMapping("customer/{customerId}")
    @Operation(tags = ["Customers"])
    override fun getCustomer(@PathVariable customerId: Long): CustomersDto {
        val customer = customersService.getCustomer(customerId)
        return customersMapper.customersToCustomersDto(customer)
    }

    @GetMapping("customers")
    @Operation(tags = ["Customers"])
    override fun getAllCustomers(pageable: Pageable): Page<CustomersDto> {
        val customerList = customersService.getAllCustomers()
        return customersMapper.customersListToCustomersListDto(customerList).toPage(pageable)
    }

    @PostMapping("customer")
    @Operation(tags = ["Customers"])
    @ResponseStatus(HttpStatus.CREATED)
    override fun addCustomer(@RequestBody createCustomersDto: CreateCustomersDto): CustomersDto {
        val customer = customersService.addCustomer(createCustomersDto)
        customer.orders = null
        return customersMapper.customersToCustomersDto(customer)
    }
}