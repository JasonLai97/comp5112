package com.comp5112.postgresDemo.controllers

import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping()
class CheckControllerImpl : CheckController {
    @GetMapping("check")
    @Operation(tags = ["Health Check"])
    override fun check(): String {
        return "It is Healthy"
    }
}