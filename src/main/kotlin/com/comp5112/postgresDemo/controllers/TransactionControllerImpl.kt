package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.TransactionDto
import com.comp5112.postgresDemo.dtos.UpdateTransactionDto
import com.comp5112.postgresDemo.mappers.TransactionMapper
import com.comp5112.postgresDemo.services.TransactionService
import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
class TransactionControllerImpl(
    private val transactionService: TransactionService,
    private val transactionMapper: TransactionMapper,
): TransactionController {
    @PutMapping("transaction/{transactionId}")
    @Operation(tags = ["Transaction Update"])
    override fun updateTransaction(
        @RequestBody updateTransactionDto: UpdateTransactionDto,
        @PathVariable transactionId: Long,
    ): TransactionDto {
        val transaction = transactionService.updateTransaction(updateTransactionDto, transactionId)
        return transactionMapper.transactionToTransactionDto(transaction)
    }
}