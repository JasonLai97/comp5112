package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.CreateOrdersDto
import com.comp5112.postgresDemo.dtos.OrdersDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface OrdersController {
    fun getOrder(orderId: Long): OrdersDto

    fun getAllOrders(pageable: Pageable): Page<OrdersDto>

    fun addOrder(createOrdersDto: CreateOrdersDto, customerId: Long): OrdersDto
}