package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.CreateOrdersDto
import com.comp5112.postgresDemo.dtos.OrdersDto
import com.comp5112.postgresDemo.mappers.OrdersMapper
import com.comp5112.postgresDemo.services.OrdersService
import com.comp5112.postgresDemo.util.toPage
import io.swagger.v3.oas.annotations.Operation
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping
class OrdersControllerImpl(
    private val ordersMapper: OrdersMapper,
    private val ordersService: OrdersService,
): OrdersController {
    @GetMapping("order/{orderId}")
    @Operation(tags = ["Orders"])
    override fun getOrder(@PathVariable orderId: Long): OrdersDto {
        val order = ordersService.getOrder(orderId)
        return ordersMapper.ordersToOrdersDto(order)
    }

    @GetMapping("orders")
    @Operation(tags = ["Orders"])
    override fun getAllOrders(pageable: Pageable): Page<OrdersDto> {
        val orderList = ordersService.getAllOrders()
        return ordersMapper.ordersListToOrdersListDto(orderList).toPage(pageable)
    }

    @PostMapping("order/by/customer/{customerId}")
    @Operation(tags = ["Orders"])
    @ResponseStatus(HttpStatus.CREATED)
    override fun addOrder(
        @RequestBody createOrdersDto: CreateOrdersDto,
        @PathVariable customerId: Long,
    ): OrdersDto {
        val order = ordersService.addOrder(createOrdersDto, customerId)
        return ordersMapper.ordersToOrdersDto(order)
    }
}