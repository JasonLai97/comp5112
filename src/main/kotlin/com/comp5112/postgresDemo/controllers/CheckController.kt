package com.comp5112.postgresDemo.controllers

interface CheckController {
    fun check(): String
}