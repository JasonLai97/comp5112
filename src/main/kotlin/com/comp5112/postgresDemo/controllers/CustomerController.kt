package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.CreateCustomersDto
import com.comp5112.postgresDemo.dtos.CustomersDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface CustomerController {
    fun getCustomer(customerId: Long): CustomersDto

    fun getAllCustomers(pageable: Pageable): Page<CustomersDto>

    fun addCustomer(createCustomersDto: CreateCustomersDto): CustomersDto
}