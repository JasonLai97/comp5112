package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.CreateDeliveryDto
import com.comp5112.postgresDemo.dtos.DeliveryDto
import com.comp5112.postgresDemo.dtos.UpdateDeliveryDto
import com.comp5112.postgresDemo.entities.Delivery
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

interface DeliveryController {
    fun getDelivery(deliveryId: Long): DeliveryDto

    fun getAllDelivery(pageable: Pageable): Page<DeliveryDto>

    fun addDelivery(createDeliveryDto: CreateDeliveryDto, transactionId: Long): DeliveryDto

    fun updateDelivery(updateDeliveryDto: UpdateDeliveryDto, deliveryId: Long): DeliveryDto
}