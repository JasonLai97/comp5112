package com.comp5112.postgresDemo.controllers

import com.comp5112.postgresDemo.dtos.CreateDeliveryDto
import com.comp5112.postgresDemo.dtos.DeliveryDto
import com.comp5112.postgresDemo.dtos.UpdateDeliveryDto
import com.comp5112.postgresDemo.mappers.DeliveryMapper
import com.comp5112.postgresDemo.services.DeliveryService
import com.comp5112.postgresDemo.util.toPage
import io.swagger.v3.oas.annotations.Operation
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping
class DeliveryControllerImpl(
    private val deliveryMapper: DeliveryMapper,
    private val deliveryService: DeliveryService,
): DeliveryController {
    @GetMapping("delivery/{deliveryId}")
    @Operation(tags = ["Delivery"])
    override fun getDelivery(@PathVariable deliveryId: Long): DeliveryDto {
        val delivery = deliveryService.getDelivery(deliveryId)
        return deliveryMapper.deliveryToDeliveryDto(delivery)
    }

    @GetMapping("deliveries")
    @Operation(tags = ["Delivery"])
    override fun getAllDelivery(pageable: Pageable): Page<DeliveryDto> {
        val deliveryList = deliveryService.getAllDelivery()
        return deliveryMapper.deliveryListToDeliveryListDto(deliveryList).toPage(pageable)
    }

    @PostMapping("delivery/for/transaction/{transactionId}")
    @Operation(tags = ["Delivery"])
    @ResponseStatus(HttpStatus.CREATED)
    override fun addDelivery(
        @RequestBody createDeliveryDto: CreateDeliveryDto,
        @PathVariable transactionId: Long,
    ): DeliveryDto {
        val delivery = deliveryService.addDelivery(createDeliveryDto, transactionId)
        return deliveryMapper.deliveryToDeliveryDto(delivery)
    }

    @PutMapping("delivery/{deliveryId}")
    @Operation(tags = ["Delivery"])
    override fun updateDelivery(
        @RequestBody updateDeliveryDto: UpdateDeliveryDto,
        @PathVariable deliveryId: Long
    ): DeliveryDto {
        val delivery = deliveryService.updateDelivery(updateDeliveryDto, deliveryId)
        return deliveryMapper.deliveryToDeliveryDto(delivery)
    }
}