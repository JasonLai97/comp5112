package com.comp5112.postgresDemo.entities

import com.comp5112.postgresDemo.entities.base.VersionEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.BatchSize
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "Delivery")
data class Delivery(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "deliveryId_seq")
    @SequenceGenerator(name = "deliveryId_seq", sequenceName = "deliveryId_seq", allocationSize = 1)
    val deliveryId: Long? = null,

    @Column(nullable = false, length = 400)
    val address: String,

    @Column(nullable = false)
    val expectedDeliveryDate: LocalDateTime,

    @Column(nullable = false)
    var actualDeliveryDate: LocalDateTime,

    @Column(nullable = false)
    var delivered: Boolean,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionId", referencedColumnName = "transactionId")
    @BatchSize(size = 100)
    @JsonBackReference
    val transaction: Transaction,
): VersionEntity() {
    @Override
    override fun toString(): String {
        return "Delivery (deliveryId=${deliveryId}" +
            "address=${address}, " +
            "expectedDeliveryDate=${expectedDeliveryDate}, " +
            "actualDeliveryDate=${actualDeliveryDate}, " +
            "delivered=${delivered}, " +
            ")"
    }
}
