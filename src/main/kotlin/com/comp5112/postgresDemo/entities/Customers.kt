package com.comp5112.postgresDemo.entities

import com.comp5112.postgresDemo.entities.base.VersionEntity
import javax.persistence.*

@Entity
@Table(name = "customers")
data class Customers(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customerId_seq")
    @SequenceGenerator(name = "customerId_seq", sequenceName = "customerId_seq", allocationSize = 1)
    val customerId: Long? = null,

    @Column(nullable = false, length = 50)
    val customerName: String,

    @Column(nullable = false)
    val vipFlg: Boolean,

    @Column(nullable = false, unique = true, length = 30)
    val phone: String,

    @Column(nullable = false, unique = true, length = 50)
    val email: String,

    @Column(nullable = false, length = 200)
    val address: String,

    @OneToMany(mappedBy = "customers", cascade = [CascadeType.ALL], orphanRemoval = true)
    var orders: MutableList<Orders>? = mutableListOf()

): VersionEntity() {
    @Override
    override fun toString(): String {
        return "Customers (customerId=${customerId}" +
            "customerName=${customerName}, " +
            "vipFlg=${vipFlg}, " +
            "phone=${phone}, " +
            "email=${email}, " +
            "address=${address}" +
            ")"
    }

}
