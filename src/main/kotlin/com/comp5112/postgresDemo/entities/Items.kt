package com.comp5112.postgresDemo.entities

import com.comp5112.postgresDemo.entities.base.VersionEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.BatchSize
import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "items")
data class Items(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "itemId_seq")
    @SequenceGenerator(name = "itemId_seq", sequenceName = "itemId_seq", allocationSize = 1)
    val itemId: Long? = null,

    @Column(nullable = false)
    val itemPrice: Long,

    @Column(nullable = true)
    val discount: BigDecimal?,

    @Column(nullable = false)
    val quantity: Int,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoiceId", referencedColumnName = "invoiceId")
    @BatchSize(size = 100)
    @JsonBackReference
    val invoice: Invoice
): VersionEntity() {
    @Override
    override fun toString(): String {
        return "Invoice (itemId=${itemId}" +
            "itemPrice=${itemPrice}, " +
            "discount=${discount}, " +
            "quantity=${quantity}, " +
            ")"
    }
}
