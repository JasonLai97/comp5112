package com.comp5112.postgresDemo.entities.base

import javax.persistence.Column
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass
import org.springframework.data.annotation.CreatedDate
import java.time.LocalDateTime

@MappedSuperclass
@EntityListeners(BaseEntityListener::class)
class BaseEntity(
    @CreatedDate
    @Column(nullable = false, columnDefinition = "timestamp")
    var updatedAt: LocalDateTime = LocalDateTime.now(),

    @Column(nullable = false, columnDefinition = "timestamp", updatable = false)
    var createdAt: LocalDateTime = LocalDateTime.now(),
)