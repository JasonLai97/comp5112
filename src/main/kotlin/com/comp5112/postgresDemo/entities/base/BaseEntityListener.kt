package com.comp5112.postgresDemo.entities.base

import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import java.time.LocalDateTime
class BaseEntityListener {
    @PreUpdate
    @PrePersist
    fun preSave(o: VersionEntity) {
        val today = LocalDateTime.now()
        o.updatedAt = today
    }
}