package com.comp5112.postgresDemo.entities.base

import com.comp5112.postgresDemo.entities.base.BaseEntity
import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.Version

@MappedSuperclass
class VersionEntity(
    @Version
    @Column(nullable = false, columnDefinition = "Int8 default 1")
    var version: Long = 0
) : BaseEntity()