package com.comp5112.postgresDemo.entities

import com.comp5112.postgresDemo.entities.base.VersionEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.BatchSize
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "invoice")
data class Invoice(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "invoiceId_seq")
    @SequenceGenerator(name = "invoiceId_seq", sequenceName = "invoiceId_seq", allocationSize = 1)
    val invoiceId: Long? = null,

    @Column(nullable = false)
    val paymentRef: Int,

    @Column(nullable = true)
    val refNo: Int?,

    @Column(nullable = false)
    val invoiceDate: LocalDateTime,

    @Column(nullable = true, length = 400)
    val remark: String?,

    @Column(nullable = false)
    val taxable: Boolean,

    @Column(nullable = true)
    val tax: BigDecimal?,

    @Column(nullable = false)
    val totalAmount: Long,

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transactionId", referencedColumnName = "transactionId")
    @BatchSize(size = 100)
    @JsonBackReference
    val transaction: Transaction,

    @OneToMany(targetEntity = Items::class, mappedBy = "invoice", cascade = [CascadeType.ALL], orphanRemoval = true)
    var items: List<Items> = mutableListOf()
): VersionEntity() {
    @Override
    override fun toString(): String {
        return "Invoice (invoiceId=${invoiceId}" +
            "paymentRef=${paymentRef}, " +
            "refNo=${refNo}, " +
            "invoiceDate=${invoiceDate}, " +
            "remark=${remark}, " +
            "taxable=${taxable}, " +
            "tax=${tax}, " +
            "totalAmount=${totalAmount}, " +
            ")"
    }
}
