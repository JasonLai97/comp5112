package com.comp5112.postgresDemo.entities

import com.comp5112.postgresDemo.entities.base.VersionEntity
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.BatchSize
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "transaction")
data class Transaction(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transactionId_seq")
    @SequenceGenerator(name = "transactionId_seq", sequenceName = "transactionId_seq", allocationSize = 1)
    val transactionId: Long? = null,

    @Column(nullable = false)
    val transactionDate: LocalDateTime,

    @Column(nullable = false, length = 20)
    val method: String,

    @Column(nullable = false)
    var pickUpFlg: Boolean,

    @Column(nullable = false)
    var deliveryFlg: Boolean,

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderId", referencedColumnName = "orderId")
    @BatchSize(size = 100)
    @JsonBackReference
    val orders: Orders,

    @OneToMany(mappedBy = "transaction", cascade = [CascadeType.ALL], orphanRemoval = true)
    val delivery: MutableList<Delivery>? = mutableListOf(),

    @OneToOne(mappedBy = "transaction", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var invoice: Invoice? = null,

    ): VersionEntity() {
    @Override
    override fun toString(): String {
        return "Transaction (transactionId=${transactionId}" +
            "transactionDate=${transactionDate}, " +
            "method=${method}, " +
            "pickUpFlg=${pickUpFlg}, " +
            "deliveryFlg=${deliveryFlg}, " +
            ")"
    }
}
