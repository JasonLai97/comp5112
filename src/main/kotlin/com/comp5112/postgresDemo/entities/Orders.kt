package com.comp5112.postgresDemo.entities

import com.comp5112.postgresDemo.entities.base.VersionEntity
import com.comp5112.postgresDemo.util.udts.StatusUdt
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.BatchSize
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "orders")
data class Orders(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orderId_seq")
    @SequenceGenerator(name = "orderId_seq", sequenceName = "orderId_seq", allocationSize = 1)
    val orderId: Long? = null,

    @Column(nullable = false)
    val orderDate: LocalDateTime,

    @Column(nullable = false)
    val status: StatusUdt.OrderStatus,

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerId", referencedColumnName = "customerId")
    @BatchSize(size = 100)
    @JsonBackReference
    val customers: Customers,

    @OneToOne(mappedBy = "orders", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var transaction: Transaction? = null,

): VersionEntity() {
    @Override
    override fun toString(): String {
        return "Orders (orderId=${orderId}" +
                "orderDate=${orderDate}, " +
                "status=${status}, " +
                ")"
    }
}
