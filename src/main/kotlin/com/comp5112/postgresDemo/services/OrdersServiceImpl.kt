package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateOrdersDto
import com.comp5112.postgresDemo.entities.Invoice
import com.comp5112.postgresDemo.entities.Items
import com.comp5112.postgresDemo.entities.Orders
import com.comp5112.postgresDemo.entities.Transaction
import com.comp5112.postgresDemo.repositories.CustomersRepository
import com.comp5112.postgresDemo.repositories.OrdersRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class OrdersServiceImpl(
    private val ordersRepository: OrdersRepository,
    private val customersRepository: CustomersRepository,
): OrdersService {

    override fun getOrder(orderId: Long): Orders {
        return ordersRepository.findByOrderId(orderId)
            ?: throw NoSuchElementException("$orderId not found")
    }

    override fun getAllOrders(): List<Orders> {
        return ordersRepository.findAll()
    }

    @Transactional
    override fun addOrder(createOrdersDto: CreateOrdersDto, customerId: Long): Orders {
        val customer = customersRepository.findByCustomerId(customerId)
            ?: throw NoSuchElementException("$customerId not found")

        val orders = Orders(
            orderDate = createOrdersDto.orderDate,
            status = createOrdersDto.status,
            customers = customer,
            transaction = null,
        )
        val transaction = Transaction(
            transactionDate = createOrdersDto.transaction.transactionDate,
            method = createOrdersDto.transaction.method,
            pickUpFlg = createOrdersDto.transaction.pickUpFlg,
            deliveryFlg = createOrdersDto.transaction.deliveryFlg,
            orders = orders,
            delivery = null,
            invoice = null,
        )
        orders.transaction = transaction

        val invoice = Invoice(
            paymentRef = createOrdersDto.transaction.invoice.paymentRef,
            refNo = createOrdersDto.transaction.invoice.refNo,
            invoiceDate = createOrdersDto.transaction.invoice.invoiceDate,
            remark = createOrdersDto.transaction.invoice.remark,
            taxable = createOrdersDto.transaction.invoice.taxable,
            tax = createOrdersDto.transaction.invoice.tax,
            totalAmount = createOrdersDto.transaction.invoice.totalAmount,
            transaction = transaction,
        )
        transaction.invoice = invoice

        val items = createOrdersDto.transaction.invoice.items?.map { itemDto ->
            Items(
                itemPrice = itemDto.itemPrice,
                discount = itemDto.discount,
                quantity = itemDto.quantity,
                invoice = invoice
            )
        } ?: mutableListOf()
        invoice.items = items

        return ordersRepository.save(orders)
    }
}