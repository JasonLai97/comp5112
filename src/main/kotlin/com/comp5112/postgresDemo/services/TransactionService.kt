package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateTransactionDto
import com.comp5112.postgresDemo.dtos.UpdateTransactionDto
import com.comp5112.postgresDemo.entities.Transaction

interface TransactionService {
    fun getTransaction(transactionId: Long): Transaction

    fun getAllTransactions(): List<Transaction>

    fun addTransaction(createTransactionDto: CreateTransactionDto): Transaction

    fun updateTransaction(updateTransactionDto: UpdateTransactionDto, transactionId: Long): Transaction
}