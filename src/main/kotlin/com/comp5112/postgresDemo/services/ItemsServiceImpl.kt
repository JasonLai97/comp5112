package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateItemsDto
import com.comp5112.postgresDemo.entities.Items
import com.comp5112.postgresDemo.mappers.ItemsMapper
import com.comp5112.postgresDemo.repositories.ItemsRepository
import org.springframework.stereotype.Service

@Service
class ItemsServiceImpl(
    private val itemsRepository: ItemsRepository,
    private val itemsMapper: ItemsMapper,
): ItemsService {
    override fun getItem(itemId: Long): Items {
        return itemsRepository.findByItemId(itemId)
            ?: throw NoSuchElementException("$itemId not found")
    }

    override fun getAllItems(): List<Items> {
        return itemsRepository.findAll()
    }

    override fun addItem(createItemsDto: CreateItemsDto): Items {
        val item = itemsMapper.createItemsDtoToItems(createItemsDto)
        return itemsRepository.save(item)
    }
}