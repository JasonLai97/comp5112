package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateInvoiceDto
import com.comp5112.postgresDemo.entities.Invoice

interface InvoiceService {
    fun getInvoice(invoiceId: Long): Invoice

    fun getAllInvoice(): List<Invoice>

    fun addInvoice(createInvoiceDto: CreateInvoiceDto): Invoice
}