package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateCustomersDto
import com.comp5112.postgresDemo.entities.Customers

interface CustomersService {
    fun getCustomer(customerId: Long): Customers

    fun getAllCustomers(): List<Customers>

    fun addCustomer(createCustomersDto: CreateCustomersDto): Customers
}