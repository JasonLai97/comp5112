package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateOrdersDto
import com.comp5112.postgresDemo.entities.Orders

interface OrdersService {
    fun getOrder(orderId: Long): Orders

    fun getAllOrders(): List<Orders>

    fun addOrder(createOrdersDto: CreateOrdersDto, customerId: Long): Orders
}