package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateItemsDto
import com.comp5112.postgresDemo.entities.Items

interface ItemsService {
    fun getItem(itemId: Long): Items

    fun getAllItems(): List<Items>

    fun addItem(createItemsDto: CreateItemsDto): Items
}