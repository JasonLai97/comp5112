package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateDeliveryDto
import com.comp5112.postgresDemo.dtos.UpdateDeliveryDto
import com.comp5112.postgresDemo.entities.Delivery

interface DeliveryService {
    fun getDelivery(deliveryId: Long): Delivery

    fun getAllDelivery(): List<Delivery>

    fun addDelivery(createDeliveryDto: CreateDeliveryDto, transactionId: Long): Delivery

    fun updateDelivery(updateDeliveryDto: UpdateDeliveryDto, deliveryId: Long): Delivery
}