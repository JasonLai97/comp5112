package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateInvoiceDto
import com.comp5112.postgresDemo.entities.Invoice
import com.comp5112.postgresDemo.mappers.InvoiceMapper
import com.comp5112.postgresDemo.repositories.InvoiceRepository
import org.springframework.stereotype.Service

@Service
class InvoiceServiceImpl(
    private val invoiceRepository: InvoiceRepository,
    private val invoiceMapper: InvoiceMapper,
): InvoiceService {
    override fun getInvoice(invoiceId: Long): Invoice {
        return invoiceRepository.findByInvoiceId(invoiceId)
            ?: throw NoSuchElementException("$invoiceId not found")
    }

    override fun getAllInvoice(): List<Invoice> {
        return invoiceRepository.findAll()
    }

    override fun addInvoice(createInvoiceDto: CreateInvoiceDto): Invoice {
        val invoice = invoiceMapper.createInvoiceDtoToInvoice(createInvoiceDto)
        return invoiceRepository.save(invoice)
    }
}