package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateCustomersDto
import com.comp5112.postgresDemo.entities.Customers
import com.comp5112.postgresDemo.mappers.CustomersMapper
import com.comp5112.postgresDemo.repositories.CustomersRepository
import org.springframework.stereotype.Service

@Service
class CustomersServiceImpl(
    private val customersRepository: CustomersRepository,
    private val customersMapper: CustomersMapper,
): CustomersService {
    override fun getCustomer(customerId: Long): Customers {
        return customersRepository.findByCustomerId(customerId)
            ?: throw NoSuchElementException("$customerId not found")
    }

    override fun getAllCustomers(): List<Customers> {
        return customersRepository.findAll()
    }

    override fun addCustomer(createCustomersDto: CreateCustomersDto): Customers {
        val customer = customersMapper.createCustomersDtoToCustomers(createCustomersDto)
        return customersRepository.save(customer)
    }

}