package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateDeliveryDto
import com.comp5112.postgresDemo.dtos.UpdateDeliveryDto
import com.comp5112.postgresDemo.entities.Delivery
import com.comp5112.postgresDemo.repositories.DeliveryRepository
import com.comp5112.postgresDemo.repositories.TransactionRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class DeliveryServiceImpl(
    private val deliveryRepository: DeliveryRepository,
    private val transactionRepository: TransactionRepository,
): DeliveryService {
    override fun getDelivery(deliveryId: Long): Delivery {
        return deliveryRepository.findByDeliveryId(deliveryId)
            ?: throw NoSuchElementException("$deliveryId not found")
    }

    override fun getAllDelivery(): List<Delivery> {
        return deliveryRepository.findAll()
    }

    @Transactional
    override fun addDelivery(createDeliveryDto: CreateDeliveryDto, transactionId: Long): Delivery {
        val transaction = transactionRepository.findByTransactionId(transactionId)
            ?: throw NoSuchElementException("$transactionId not found")

        val delivery = Delivery(
            address = createDeliveryDto.address,
            expectedDeliveryDate = createDeliveryDto.expectedDeliveryDate,
            actualDeliveryDate = createDeliveryDto.actualDeliveryDate,
            delivered = createDeliveryDto.delivered,
            transaction = transaction,
        )

        return deliveryRepository.save(delivery)
    }

    override fun updateDelivery(updateDeliveryDto: UpdateDeliveryDto, deliveryId: Long): Delivery {
        val delivery = deliveryRepository.findByDeliveryId(deliveryId)
            ?: throw NoSuchElementException("$deliveryId not found")

        delivery.actualDeliveryDate = updateDeliveryDto.actualDeliveryDate
        delivery.delivered = updateDeliveryDto.delivered

        return deliveryRepository.save(delivery)
    }
}