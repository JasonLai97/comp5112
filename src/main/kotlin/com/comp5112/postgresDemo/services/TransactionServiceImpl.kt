package com.comp5112.postgresDemo.services

import com.comp5112.postgresDemo.dtos.CreateTransactionDto
import com.comp5112.postgresDemo.dtos.UpdateTransactionDto
import com.comp5112.postgresDemo.entities.Transaction
import com.comp5112.postgresDemo.mappers.TransactionMapper
import com.comp5112.postgresDemo.repositories.TransactionRepository
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class TransactionServiceImpl(
    private val transactionRepository: TransactionRepository,
    private val transactionMapper: TransactionMapper,
): TransactionService {
    override fun getTransaction(transactionId: Long): Transaction {
        return transactionRepository.findByTransactionId(transactionId)
            ?: throw NoSuchElementException("$transactionId not found")
    }

    override fun getAllTransactions(): List<Transaction> {
        return transactionRepository.findAll()
    }

    override fun addTransaction(createTransactionDto: CreateTransactionDto): Transaction {
        val transaction = transactionMapper.createTransactionDtoToTransaction(createTransactionDto)
        return transactionRepository.save(transaction)
    }

    @Transactional
    override fun updateTransaction(updateTransactionDto: UpdateTransactionDto, transactionId: Long): Transaction {
        val transaction = transactionRepository.findByTransactionId(transactionId)
            ?: throw NoSuchElementException("$transactionId not found")

        transaction.pickUpFlg = updateTransactionDto.pickUpFlg
        transaction.deliveryFlg = updateTransactionDto.deliveryFlg

        return transactionRepository.save(transaction)
    }
}