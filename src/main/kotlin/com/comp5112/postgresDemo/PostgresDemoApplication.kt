package com.comp5112.postgresDemo

import com.comp5112.postgresDemo.config.JDBCDbConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PostgresDemoApplication {
	val log: Logger = LoggerFactory.getLogger(javaClass)
}

fun main(args: Array<String>) {
	val context = runApplication<PostgresDemoApplication>(*args)
	val jdbcDbConfig = context.getBean(JDBCDbConfig::class.java)

	val postgresDemoApplication = PostgresDemoApplication()

	Runtime.getRuntime().addShutdownHook(object : Thread() {
		override fun run() {
			postgresDemoApplication.log.info("Application is shutting down...")
			val connection = jdbcDbConfig.getConnection()
			connection.clearWarnings()
			connection.createStatement().apply {
				clearBatch()
				close()
			}
			connection.close()
			postgresDemoApplication.log.info("Batch cleared, db connection close...")
		}
	})
}
