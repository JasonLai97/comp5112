package com.comp5112.postgresDemo.util

class LogStr {
    companion object {
        const val ATTEMPT_LOG_PREFIX = "Attempting to"
        const val COMPLETE_LOG_PREFIX = "Successfully"
    }

}