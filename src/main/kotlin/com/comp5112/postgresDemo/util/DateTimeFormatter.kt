package com.comp5112.postgresDemo.util

import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.Date
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

@Component
class DateTimeFormatter() {
    private val dateFormat = "yyyy-MM-dd"
    private val timeFormat = "HH:mm:ss"
    private val dateTimeFormat = "$dateFormat $timeFormat"

    fun getCurrentTimeString(): String {
        val dateFormat = SimpleDateFormat(dateTimeFormat)
        val currentTime = Date()
        return dateFormat.format(currentTime)
    }

    fun formatStrToDateTime(dateTimeString: String): LocalDateTime {
        val formatter = DateTimeFormatter.ofPattern(dateTimeFormat)
        return LocalDateTime.parse(dateTimeString, formatter)
    }

    fun formatDateToStr(date: LocalDate): String {
        val formatter = DateTimeFormatter.ofPattern(dateFormat)
        return date.format(formatter)
    }

    fun formatTimeToStr(time: LocalTime): String {
        val formatter = DateTimeFormatter.ofPattern(timeFormat)
        return time.format(formatter)
    }

    fun formatTimestamp(timestamp: Long): String {
        val instant = Instant.ofEpochMilli(timestamp)
        val formatter = DateTimeFormatter.ofPattern(dateTimeFormat).withZone(ZoneId.systemDefault())
        return formatter.format(instant)
    }
}