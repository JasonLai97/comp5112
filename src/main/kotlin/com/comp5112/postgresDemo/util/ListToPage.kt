package com.comp5112.postgresDemo.util

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import kotlin.math.min

fun <T> List<T>.toPage(pageable: Pageable): Page<T> {
    val totalElements = this.size.toLong()
    val pageStart = pageable.offset
    val pageSize = pageable.pageSize.toLong()
    val pageEnd = min(pageStart + pageSize, totalElements)
    return PageImpl(this.subList(pageStart.toInt(), pageEnd.toInt()), pageable, totalElements)
}