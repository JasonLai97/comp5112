package com.comp5112.postgresDemo.util.udts

import io.swagger.v3.oas.annotations.media.Schema

open class StatusUdt(){
    @Schema(enumAsRef = true)
    enum class OrderStatus {
        delivered,
        pending,
        processing,
        rejected,
    }

    @Schema(enumAsRef = true)
    enum class TransactionStatus {
        online,
        bank,
        crash,
    }
}