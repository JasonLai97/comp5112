package com.comp5112.postgresDemo.mappers

import com.comp5112.postgresDemo.dtos.CreateOrdersDto
import com.comp5112.postgresDemo.dtos.OrdersDto
import com.comp5112.postgresDemo.entities.Orders
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface OrdersMapper {
    fun ordersToOrdersDto(entity: Orders): OrdersDto

    fun ordersListToOrdersListDto(list: List<Orders>): List<OrdersDto>

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "version", ignore = true)
    fun createOrdersDtoToOrders(createOrdersDto: CreateOrdersDto): Orders
}