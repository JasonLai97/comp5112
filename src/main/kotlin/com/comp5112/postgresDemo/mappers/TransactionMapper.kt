package com.comp5112.postgresDemo.mappers

import com.comp5112.postgresDemo.dtos.CreateTransactionDto
import com.comp5112.postgresDemo.dtos.TransactionDto
import com.comp5112.postgresDemo.entities.Transaction
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface TransactionMapper {
    fun transactionToTransactionDto(entity: Transaction): TransactionDto

    fun transactionListToTransactionListDto(list: List<Transaction>): List<TransactionDto>

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "version", ignore = true)
    fun createTransactionDtoToTransaction(createTransactionDto: CreateTransactionDto): Transaction
}