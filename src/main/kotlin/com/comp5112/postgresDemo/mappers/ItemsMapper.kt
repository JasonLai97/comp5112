package com.comp5112.postgresDemo.mappers

import com.comp5112.postgresDemo.dtos.CreateItemsDto
import com.comp5112.postgresDemo.dtos.ItemsDto
import com.comp5112.postgresDemo.entities.Items
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface ItemsMapper {
    fun itemsToItemsDto(entity: Items): ItemsDto

    fun itemsListToItemsListDto(list: List<Items>): List<ItemsDto>

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "version", ignore = true)
    fun createItemsDtoToItems(createItemsDto: CreateItemsDto): Items
}