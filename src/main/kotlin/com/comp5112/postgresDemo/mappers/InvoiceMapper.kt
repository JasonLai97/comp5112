package com.comp5112.postgresDemo.mappers

import com.comp5112.postgresDemo.dtos.CreateInvoiceDto
import com.comp5112.postgresDemo.dtos.InvoiceDto
import com.comp5112.postgresDemo.entities.Invoice
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface InvoiceMapper {
    fun invoiceToInvoiceDto(entity: Invoice): InvoiceDto

    fun invoiceListToInvoiceListDto(list: List<Invoice>): List<InvoiceDto>

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "version", ignore = true)
    fun createInvoiceDtoToInvoice(createInvoiceDto: CreateInvoiceDto): Invoice
}