package com.comp5112.postgresDemo.mappers

import com.comp5112.postgresDemo.dtos.CreateDeliveryDto
import com.comp5112.postgresDemo.dtos.DeliveryDto
import com.comp5112.postgresDemo.entities.Delivery
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface DeliveryMapper {
    fun deliveryToDeliveryDto(entity: Delivery): DeliveryDto

    fun deliveryListToDeliveryListDto(list: List<Delivery>): List<DeliveryDto>

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "version", ignore = true)
    fun createDeliveryDtoToDelivery(createDeliveryDto: CreateDeliveryDto): Delivery
}