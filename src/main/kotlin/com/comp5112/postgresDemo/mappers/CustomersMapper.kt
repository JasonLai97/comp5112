package com.comp5112.postgresDemo.mappers

import com.comp5112.postgresDemo.dtos.CreateCustomersDto
import com.comp5112.postgresDemo.dtos.CustomersDto
import com.comp5112.postgresDemo.entities.Customers
import org.mapstruct.Mapper
import org.mapstruct.Mapping

@Mapper(componentModel = "spring")
interface CustomersMapper {
    fun customersToCustomersDto(entity: Customers): CustomersDto

    fun customersListToCustomersListDto(list: List<Customers>): List<CustomersDto>

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "version", ignore = true)
    fun createCustomersDtoToCustomers(createCustomersDto: CreateCustomersDto): Customers
}