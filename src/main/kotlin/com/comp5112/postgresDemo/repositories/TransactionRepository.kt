package com.comp5112.postgresDemo.repositories

import com.comp5112.postgresDemo.entities.Transaction
import org.springframework.data.jpa.repository.JpaRepository

interface TransactionRepository : JpaRepository<Transaction, Long> {
    fun findByTransactionId(transactionId: Long): Transaction?
}