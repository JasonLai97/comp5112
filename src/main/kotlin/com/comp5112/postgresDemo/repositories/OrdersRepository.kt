package com.comp5112.postgresDemo.repositories

import com.comp5112.postgresDemo.entities.Orders
import org.springframework.data.jpa.repository.JpaRepository

interface OrdersRepository : JpaRepository<Orders, Long> {
    fun findByOrderId(orderId: Long): Orders?
}