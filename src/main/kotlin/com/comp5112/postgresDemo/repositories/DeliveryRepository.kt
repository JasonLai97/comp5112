package com.comp5112.postgresDemo.repositories

import com.comp5112.postgresDemo.entities.Delivery
import org.springframework.data.jpa.repository.JpaRepository

interface DeliveryRepository : JpaRepository<Delivery, Long> {
    fun findByDeliveryId(deliveryId: Long): Delivery?
}