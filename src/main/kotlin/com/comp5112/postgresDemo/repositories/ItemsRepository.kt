package com.comp5112.postgresDemo.repositories

import com.comp5112.postgresDemo.entities.Items
import org.springframework.data.jpa.repository.JpaRepository

interface ItemsRepository : JpaRepository<Items, Long> {
    fun findByItemId(itemId: Long): Items?
}