package com.comp5112.postgresDemo.repositories

import com.comp5112.postgresDemo.entities.Invoice
import org.springframework.data.jpa.repository.JpaRepository

interface InvoiceRepository : JpaRepository<Invoice, Long> {
    fun findByInvoiceId(invoiceId: Long): Invoice?
}