package com.comp5112.postgresDemo.repositories

import com.comp5112.postgresDemo.entities.Customers
import org.springframework.data.jpa.repository.JpaRepository

interface CustomersRepository : JpaRepository<Customers, Long> {
    fun findByCustomerId(customerId: Long): Customers?
}