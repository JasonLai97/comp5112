# COMP5112 
Prepared by **LAI Kin Wai (23000801g)**

COMP5112 **postgreSQL** demo.

Endpoint (AWS EC2):

http://18.220.92.185:8000/api/v1/comp5112/swagger-ui/index.html#/

## Environment
- JAVA 11
- Postgres 16.X

## Project Framework
- Kotlin Language
- Spring-boot (JPA with Hibernate)
- postgreSQL

## Run Project
1. Install **IntelliJ** 
2. make sure you install maven and add it to you system environment variable
3. Install PostgreSQL and pgadmin 4 
4. Create the database with current _application.yaml_
```
  datasource:
    url: jdbc:postgresql://${DB_HOST:localhost}:${DB_PORT:5432}/${DB_NAME:postgres}
    username: ${DB_USERNAME:postgres}
    password: ${DB_PASSWORD:su123}
```
5. install JDK 11, set to use it in project structure
6. run `mvn clean install` in terminal
7. edit run config, choose Maven, in Run input box: `spring-boot:run -Dspring-boot.run.fork=false -f pom.xml`

## Test APIs
Use swagger to test the APIs. You can use postman too if you want.
1. go to http://localhost:8000/api/v1/comp5112/swagger-ui/index.html
2. Click **Try it out** button and **Execute** to see the result.

## Deploy Steps
1. build the image (#2 is force platform)
```
docker build -t comp5112-postgresql .
```
```
docker buildx build --platform linux/amd64 -t comp5112-postgresql .
``` 
2. `docker save -o comp5112.tar comp5112-postgresql:latest`
3. upload the .tar to server, then `docker load --input comp5112.tar`
4. run the container
```
docker run -p 8000:8000 
--env CORS_ENABLE=true 
--env DB_HOST=<ip_address> 
--env DB_PORT=5432 
--env DB_NAME=postgres 
--env DB_PASSWORD=<db_password> 
--env DB_USERNAME=postgres 
--env SWAGGER_HOST=<ip_address> 
--name comp5112-postgresql 
comp5112-postgresql:latest
```
